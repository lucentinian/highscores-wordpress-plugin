# highscores-wordpress-plugin

This plugin provides the highscores tables as widgets in the public domain pages using the existing library https://packagist.org/packages/ehehdada/highscore-tables-php-client-library 

In the admin pages, user can add tables by giving table_name, table_key, and the public shared_secret. Registered tables can be also removed from the admin pages.